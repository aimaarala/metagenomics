#!/usr/bin/env bash

#See below to set yarn and executor memory options right
#http://stackoverflow.com/questions/38331502/spark-on-yarn-resource-manager-relation-between-yarn-containers-and-spark-execu
#example parameters
#./split_n_blast.sh /Projects/NGS_Projects/previous_fatsas/2015_F2_20160119.fa /Projects/NGS_Projects/test_outputs 1000 2015_F2_20160119

DEPLOY_MODE=client
SCHEDULER_MODE=FAIR
CLASSPATH=metagenomics-0.9-jar-with-dependencies.jar
INPUT_FILE=${1} #path to HDFS input fasta file
OUTPUT_PATH=${2} #path to HDFS output directory
NUMBER_OF_PARTITIONS=${3} #Used for directory name suffix
PROJECT_NAME=${4} #Used for directory name suffix
BLAST_TASK=megablast #other option is blastn which is default
BLAST_DATABASE=/mnt/hdfs/1/Index_blastn/nt #path to local fs

#EX_MEM=${4}
#NUM_EX=${7}
#EX_CORES=${8}

#--conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100
#Decompress and interleave all data from HDFS path
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 10g --conf spark.yarn.executor.memoryOverhead=5000   --class fi.aalto.ngs.seqspark.metagenomics.SplitFasta metagenomics-0.9-jar-with-dependencies.jar -in ${INPUT_FILE} -out ${OUTPUT_PATH}/${PROJECT_NAME}_splits -partitions ${NUMBER_OF_PARTITIONS}

#Blast per assembled sample contig file, megablast used
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 20g --conf spark.yarn.executor.memoryOverhead=10000  --class fi.aalto.ngs.seqspark.metagenomics.BlastNFiles metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_splits -out ${OUTPUT_PATH}/${PROJECT_NAME}_blast -db ${BLAST_DATABASE} -task ${BLAST_TASK} -outfmt 6
