package fi.aalto.ngs.seqspark.metagenomics;

import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.seqdoop.hadoop_bam.FastqInputFormat;
import org.seqdoop.hadoop_bam.FastqOutputFormat;
import org.seqdoop.hadoop_bam.SequencedFragment;
import scala.Tuple2;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by davbzh on 2017-04-14.
 */
public class FastqToFasta {

    public static void main(String[] args) throws IOException {

        if (args.length < 1) {
            System.err.println("Usage: FastqToFasta <input path> <output path> <number of partitions>");
            System.exit(1);
        }

        SparkConf conf = new SparkConf().setAppName("FastqToFasta");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaPairRDD<Text, SequencedFragment> fastqRDD = sc.newAPIHadoopFile(args[0], FastqInputFormat.class, Text.class, SequencedFragment.class, sc.hadoopConfiguration());

        JavaRDD<String> fastaRDD = fastqRDD.map(fq -> {
            return ">"+fq._1.toString()+"\n"+fq._2().getSequence().toString();
        });

        fastaRDD.saveAsTextFile(args[1]);

        sc.stop();
    }
}
