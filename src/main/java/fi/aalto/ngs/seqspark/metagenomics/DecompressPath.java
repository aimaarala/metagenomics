package fi.aalto.ngs.seqspark.metagenomics;


import org.apache.commons.cli.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.NLineInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.seqdoop.hadoop_bam.FastqInputFormat.FastqRecordReader;
import org.seqdoop.hadoop_bam.FastqOutputFormat;
import org.seqdoop.hadoop_bam.SequencedFragment;
import scala.Tuple3;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**Usage
 * spark-submit --master local[15] --class fi.aalto.ngs.seqspark.metagenomics.Interleave target/metagenomics-0.9-jar-with-dependencies.jar -fastq /user/root/reads/reads/forward.fastq -fastq2 /user/root/reads/reads/reverse.fastq -splitsize 100000 -splitout /user/root/interleaved2 -interleave
 **/

public class DecompressPath {

  //Example run from examples/target dir
  //su hdfs
  //unset SPARK_JAVA_OPTS

  //TODO: change to not static, might cause exceptions with Yarn


  public DecompressPath() {
  }

  public static void main(String[] args) throws IOException {
    SparkConf conf = new SparkConf().setAppName("DecompressPath");
    //conf.set("spark.scheduler.mode", "FAIR");
    //conf.set("spark.scheduler.allocation.file", "/opt/cloudera/parcels/CDH-5.10.0-1.cdh5.10.0.p0.41/etc/hadoop/conf.dist/pools.xml");
    JavaSparkContext sc = new JavaSparkContext(conf);
    //sc.setLocalProperty("spark.scheduler.pool", "production");

    //String query = args[2];

    Options options = new Options();

    Option splitDirOpt = new Option( "out", true, "Path to output directory in hdfs." );
    Option numsplitsOpt = new Option( "splitsize", true, "Number of reads in split, depends on the size of read file, number of cores and available memory." );
    options.addOption( new Option( "temp", true, "" ) );
    options.addOption( new Option( "in", true, "" ) );
    options.addOption( new Option( "remtemp", "" ) );

    options.addOption( numsplitsOpt );
    options.addOption( splitDirOpt );
    options.addOption(new Option( "help", "print this message" ));

    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "spark-submit <spark specific args>", options, true );

    CommandLineParser parser = new BasicParser();
    CommandLine cmd = null;
    try {
      // parse the command line arguments
      cmd = parser.parse( options, args );

    }
    catch( ParseException exp ) {
      // oops, something went wrong
      System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
    }
    String input = (cmd.hasOption("in")==true)? cmd.getOptionValue("in"):null;
    String outpath = cmd.getOptionValue("out");

    FileSystem fs = FileSystem.get(new Configuration());

          FileStatus[] st = fs.listStatus(new Path(input));

          ArrayList<Tuple3<String, String, Integer>> splitFileList = new ArrayList<>();

          ArrayList<FileStatus> compressedfiles = new ArrayList<>();
          for (FileStatus f : Arrays.asList(st)) {
            if (f.getPath().getName().endsWith(".gz") || f.getPath().getName().endsWith(".zip") || f.getPath().getName().endsWith(".tar") || f.getPath().getName().endsWith(".bz"))
              compressedfiles.add(f);
          }

          Iterator<FileStatus> it = compressedfiles.iterator();
          int count = 1;
          while (it.hasNext()) {
            String fn1 = it.next().getPath().getName();
            if (it.hasNext()) {
              String fn2 = it.next().getPath().getName();
              splitFileList.add(new Tuple3<>(fn1, fn2, count));
              count++;
            }
          }

          JavaRDD<Tuple3<String, String, Integer>> filesRDD = sc.parallelize(splitFileList, splitFileList.size());
          filesRDD.foreachPartition(files -> {
            Tuple3<String, String, Integer> f = files.next();
            decompress(FileSystem.get(new Configuration()), input + "/" + f._1(), outpath + "/" + f._3() + "/1.fq");
            decompress(FileSystem.get(new Configuration()), input + "/" + f._2(), outpath + "/" + f._3() + "/2.fq");

          });

      /*if(fastq.startsWith("file://")){
        //TODO: treat local files differently, move to hdfs or split by external commands
        File f = new File(fastq);
      }else{

      }*/
      //Count split positions

    sc.stop();

  }

  private static void decompress(FileSystem fs, String in, String outpath) throws IOException {
    //TODO: handle compressed files, use LZO if possible as it is splittable format
    Configuration conf = new Configuration();
    CompressionCodecFactory factory = new CompressionCodecFactory(conf);
    // the correct codec will be discovered by the extension of the file

    CompressionCodec codec = factory.getCodec(new Path(in));
    //Decompressing zip file.
    InputStream is = codec.createInputStream(fs.open(new Path(in)));
    OutputStream out = fs.create(new Path(outpath));
    //Write decompressed out
    IOUtils.copyBytes(is, out, conf);
    is.close();
    out.close();
  }


  private static void splitFastq(FileStatus fst, String fqPath, String splitDir, int splitlen, JavaSparkContext sc) throws IOException {
    Path fqpath = new Path(fqPath);
    String fqname = fqpath.getName();
    String[] ns = fqname.split("\\.");
    //TODO: Handle also compressed files
    List<FileSplit> nlif = NLineInputFormat.getSplitsForFile(fst, sc.hadoopConfiguration(), splitlen);

    JavaRDD<FileSplit> splitRDD = sc.parallelize(nlif);

    splitRDD.foreach( split ->  {

      FastqRecordReader fqreader = new FastqRecordReader(new Configuration(), split);
      writeFastqFile(fqreader, new Configuration(), splitDir + "/split_" + split.getStart() + "." + ns[1]);

     });
  }

  private static void writeInterleavedSplits(FastqRecordReader fqreader, FastqRecordReader fqreader2, Configuration config, String fileName){

    ByteArrayOutputStream os = new ByteArrayOutputStream();

    FSDataOutputStream dataOutput = null;
    try {
      FileSystem fs = FileSystem.get(config);
      dataOutput = new FSDataOutputStream(os);
      dataOutput = fs.create(new Path(fileName));
    } catch (IOException e) {
      e.printStackTrace();
    }

    FastqOutputFormat.FastqRecordWriter writer = new FastqOutputFormat.FastqRecordWriter(config, dataOutput);

    try {
      boolean next = true;
      while(next) {
        Text t = fqreader.getCurrentKey();
        SequencedFragment sf = fqreader.getCurrentValue();
        next = fqreader.next(t, sf);

        if(next) writer.write(fqreader.getCurrentKey(), fqreader.getCurrentValue());

        Text t2 = fqreader2.getCurrentKey();
        SequencedFragment sf2 = fqreader2.getCurrentValue();
        fqreader2.next(t2, sf2);

        if(next) writer.write(fqreader2.getCurrentKey(), fqreader2.getCurrentValue());

      }

      dataOutput.close();
      os.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void writeFastqFile(FastqRecordReader fqreader, Configuration config, String fileName){

    ByteArrayOutputStream os = new ByteArrayOutputStream();

    FSDataOutputStream dataOutput = null;
    try {
      FileSystem fs = FileSystem.get(config);
      dataOutput = new FSDataOutputStream(os);
      dataOutput = fs.create(new Path(fileName));
    } catch (IOException e) {
      e.printStackTrace();
    }

    FastqOutputFormat.FastqRecordWriter writer = new FastqOutputFormat.FastqRecordWriter(config, dataOutput);

    try {
      boolean next = true;
      while(next) {
        Text t = fqreader.getCurrentKey();
        SequencedFragment sf = fqreader.getCurrentValue();
        next = fqreader.next(t, sf);
        writer.write(fqreader.getCurrentKey(), fqreader.getCurrentValue());
      }

      dataOutput.close();
      os.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void interleaveSplitFastq(FileStatus fst, FileStatus fst2, String splitDir, int splitlen, JavaSparkContext sc) throws IOException {

    String[] ns = fst.getPath().getName().split("\\.");
    //TODO: Handle also compressed files
    List<FileSplit> nlif = NLineInputFormat.getSplitsForFile(fst, sc.hadoopConfiguration(), splitlen);
    List<FileSplit> nlif2 = NLineInputFormat.getSplitsForFile(fst2, sc.hadoopConfiguration(), splitlen);

    JavaRDD<FileSplit> splitRDD = sc.parallelize(nlif);
    JavaRDD<FileSplit> splitRDD2 = sc.parallelize(nlif2);
    JavaPairRDD<FileSplit, FileSplit> zips = splitRDD.zip(splitRDD2);

    zips.foreach( splits ->  {
      Path path = splits._1.getPath();
      FastqRecordReader fqreader = new FastqRecordReader(new Configuration(), splits._1);
      FastqRecordReader fqreader2 = new FastqRecordReader(new Configuration(), splits._2);
      //name filesplits by path and name (numeric)
      writeInterleavedSplits(fqreader, fqreader2, new Configuration(), splitDir+"/"+path.getParent().getName()+"_"+splits._1.getStart()+".fq");
    });
  }

 private static FastqRecordReader readFastqSplits(String fastqfile, Configuration config, long start, long length) throws IOException {

    FileSplit split = new FileSplit(new Path(fastqfile), start, length, null);

    return new FastqRecordReader(config, split);
  }


  //split fastq cmd way
    /*Process lines = new ProcessBuilder("/bin/sh", "-c", "hdfs dfs -cat "+fastq+" | wc -l ").start();
    BufferedReader brl = new BufferedReader(new InputStreamReader(lines.getInputStream()));
    String sl ="";
    String line;
    while ((line = brl.readLine()) != null) {
      System.out.println("LINES!:"+line);
      sl += line;
    }
    int lc = Integer.parseInt(sl);
    int slc = lc/splits;
    int splitsize = Math.floorMod(slc, 4);

    ArrayList<Integer> splitnum = new ArrayList<>();
    for (int i = 0; i<=splits; i++)
      splitnum.add(i);*/
}