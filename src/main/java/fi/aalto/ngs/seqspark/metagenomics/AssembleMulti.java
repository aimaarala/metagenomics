package fi.aalto.ngs.seqspark.metagenomics;/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.commons.cli.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**Usage
 * UNSET SPARK_JAVA_OPTS with YARN!
  Depending on the number of partitions, this gives more output than if reads are processed sequentially.
  Closely similar output to sequential can be obtained by tuning input parameters
 Took only 10s with 15 cores 5G ram in each per sequenced individual (4G unmaapped reads)
 Run local:
 > spark-submit  --master local[15] --executor-memory 5g --class fi.aalto.ngs.seqspark.metagenomics.normalization.NormalizeReads metagenomics-0.9-jar-with-dependencies.jar -fastq fqsplits -out normalized

 YARN needs FULL PATH to files
 Do not use dynamic allocation with this
  --conf spark.dynamicAllocation.enabled=false
 Be sure that there is enough available cores and memory for each partition!
 See for tuning http://blog.cloudera.com/blog/2015/03/how-to-tune-your-apache-spark-jobs-part-2/

 Run on Yarn cluster:

  > spark-submit --master yarn --deploy-mode client --conf spark.dynamicAllocation.enabled=true --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --class fi.aalto.ngs.seqspark.metagenomics.NormalizeParts metagenomics-0.9-jar-with-dependencies.jar -in /user/root/temp/1/out -temp /user/root/t -out /user/root/normalized -samplenum 1 -repartition 1 -p
 **/


public class AssembleMulti {

  //Example run from examples/target dir
  //su hdfs
  //unset SPARK_JAVA_OPTS

  public static void main(String[] args) throws IOException {
    SparkConf conf = new SparkConf().setAppName("Assemble");
    JavaSparkContext sc = new JavaSparkContext(conf);

    //String query = args[2];
    //System.setProperty("SPARK_JAVA_OPTS", null);

    Options options = new Options();

    //gmOpt.setRequired(true);

    Option pairedOpt = new Option( "paired", "Use paired end reads" );
    Option splitOpt = new Option( "in", true, "" );
    //-C 15 -k 19 -N 4 -x 1e+8
    Option cOpt = new Option( "t", true, "Threads" );
    Option kOpt = new Option( "m", true, "fraction of memory to be used per process" );
    Option ouOpt = new Option( "out", true, "Path to hdfs" );
    options.addOption(new Option( "dw", "Write directly to hdfs without Spark partitioning" ));
    options.addOption(new Option( "repartition", true,  "number of partitions" ));
    options.addOption(new Option( "samplenum", true, "index of sample sequence"));
    options.addOption(new Option( "localdir", true, "Absolute path to local temp dir"));

    options.addOption( pairedOpt );
    options.addOption( splitOpt );
    options.addOption( cOpt );
    options.addOption( kOpt );
    options.addOption( ouOpt );

    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "spark-submit <spark specific args>", options, true );

    CommandLineParser parser = new BasicParser();
    CommandLine cmd = null;
    try {
      // parse the command line arguments
      cmd = parser.parse( options, args );

    }
    catch( ParseException exp ) {
      // oops, something went wrong
      System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
    }
    String splitDir = (cmd.hasOption("in")==true)? cmd.getOptionValue("in"):null;
    String outDir = (cmd.hasOption("out")==true)? cmd.getOptionValue("out"):null;
    int numpartitions = (cmd.hasOption("repartition")==true)? Integer.valueOf(cmd.getOptionValue("repartition")):0;
    String localdir = (cmd.hasOption("localdir")==true)? cmd.getOptionValue("localdir"):null;
    String samplenum = cmd.getOptionValue("samplenum");

    //boolean paired = cmd.hasOption("paired");
    int t = (cmd.hasOption("t")==true)? Integer.valueOf(cmd.getOptionValue("t")):1;
    double m = (cmd.hasOption("m")==true)? Double.valueOf(cmd.getOptionValue("m")):0.1;



    //TODO: MAKE SURE  THAT READS are LONGER than value K!! should be filtered or shortest length reported in alignment phase!
    /*if(numpartitions!=0){
      sc.textFile(splitDir).coalesce(numpartitions).saveAsTextFile(tempdir+"/"+samplenum);
      splitDir=tempdir+"/"+samplenum;
    }*/

    final String finalSplitDir = splitDir;
    FileSystem fs = FileSystem.get(new Configuration());

    //TODO: handle files from separate diretories
    FileStatus[] dirs = fs.listStatus(new Path(splitDir));
    ArrayList<String> splitFileList = new ArrayList<>();
    for (FileStatus dir : dirs){
      FileStatus[] st = fs.listStatus(dir.getPath());
      for (int i=0;i<st.length;i++){
        String fn = st[i].getPath().getName().toString();
        if(!fn.equalsIgnoreCase("_SUCCESS")){
          splitFileList.add(dir.getPath().toString()+"/"+fn);
          System.out.println(dir.getPath().toString()+"/"+fn);
        }
      }
    }
    JavaRDD<String> splitFilesRDD = sc.parallelize(splitFileList, splitFileList.size());

    splitFilesRDD.foreachPartition(f -> {
        String fname = f.next();
        String[] split = fname.split("/");
        String postfix = split[split.length-2];
        //hdfs dfs -text /user/root/ramdisk/bwa/splits/split_0.fq | normalize-by-median.py -C 15 -k 19 -N 4 -x 1e+8 -o <(hdfs dfs -put /dev/stdin /user/root/normalized/normalized_split_0.fq)
        //TODO: If yarn used, put directly to HDFS
        //megahit -t2 -m0.1 -r /root/normed.fq -o /root/ass
        String normalize_cmd = "/srv/hops/hadoop/bin/hdfs dfs -text " + fname + " | /srv/non_hdfs/megahit/megahit -t" + t + " -m" + m + " -r /dev/stdin -o "+localdir+"/"+postfix;
        System.out.println(normalize_cmd);
        //this is right way, Runtime.getRuntime().exec(command) does not work as InputStream is not piped
        ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", normalize_cmd);
        Process process = pb.start();
        BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String e;
        while ((e = err.readLine()) != null) {
          System.out.println(e);
        }
        process.waitFor();

    });

    fs.copyFromLocalFile(new Path(localdir), new Path(outDir));
    //TODO: convert to fastq rdds before writing.. no need if data already partitioned so that read does not break
    /*if(format!=null)
      new HDFSWriter(outRDD, outDir, format, sc);
    else*/

    /*if(numpartitions!=0)
      fs.delete(new Path(splitDir), true);*/
    sc.stop();

  }

}