package fi.aalto.ngs.seqspark.metagenomics;


import com.github.lindenb.jbwa.jni.BwaIndex;
import com.github.lindenb.jbwa.jni.BwaMem;
import com.github.lindenb.jbwa.jni.ShortRead;
import org.apache.commons.cli.*;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.launcher.SparkAppHandle;
import org.apache.spark.launcher.SparkLauncher;
import org.seqdoop.hadoop_bam.FastqInputFormat;
import org.seqdoop.hadoop_bam.SequencedFragment;
import scala.Tuple2;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**Usage
 *  spark-submit --master local[4] --class fi.aalto.ngs.seqspark.metagenomics.AlignNFilterOnlyUnmapped target/metagenomics-0.9-jar-with-dependencies.jar -fastq /user/root/fqsplits -ref /index/adhoc.fa -out /user/root/testb

 **/


public class MultiJobExecutor {

  //unset SPARK_JAVA_OPTS

  public static void main(String[] args) throws IOException {

    String mode = args[0];
    int start = Integer.valueOf(args[1]);
    int njobs = Integer.valueOf(args[2]);
    String splitsize = args[3];
    ExecutorService executorService = Executors.newFixedThreadPool(njobs);

    for(int k = start; k<=start+njobs; k++){
      final int i = k;
      executorService.submit(new Callable<Long>() {
        @Override
        public Long call() throws Exception {
           SparkLauncher sl = new SparkLauncher();
            sl.setVerbose(true)
                    .setMaster("yarn")
                    .setDeployMode(mode)
                    .setConf("spark.scheduler.mode", "FAIR")
                    .setConf("spark.yarn.queue", "default")
                    .setConf("spark.shuffle.service.enabled","true")
                    .setConf("spark.dynamicAllocation.enabled","true")
                    .setConf("spark.task.maxFailures","100")
                    .setConf("spark.yarn.max.executor.failures","100")
                    .setMainClass("fi.aalto.ngs.seqspark.metagenomics.Interleave")
                    .setAppResource("metagenomics-0.9-jar-with-dependencies.jar")
                    .addAppArgs("/Projects/NGS_Projects/test_outputs/2011_G5_96ALL-HiSeq_decompressed/"+ i +"/1.fq","/Projects/NGS_Projects/test_outputs/2011_G5_96ALL-HiSeq_decompressed/"+i+"/2.fq", "/Projects/NGS_Projects/test_outputs/interleaved_per_sample/"+i, splitsize);
             sl.launch();

          Process spark = sl.launch();
         /* InputStreamReaderRunnable inputStreamReaderRunnable = new InputStreamReaderRunnable(spark.getInputStream(), "input");
          Thread inputThread = new Thread(inputStreamReaderRunnable, "LogStreamReader input");
          inputThread.start();

          InputStreamReaderRunnable errorStreamReaderRunnable = new InputStreamReaderRunnable(spark.getErrorStream(), "error");
          Thread errorThread = new Thread(errorStreamReaderRunnable, "LogStreamReader error");
          errorThread.start();
*/
          new Thread(new Runnable() { public void run() {
            BufferedReader in = new BufferedReader(new InputStreamReader(spark.getErrorStream()));
            String line;
            try {
              while ((line = in.readLine()) != null) {
                System.out.println(line);
              }
            } catch (IOException e) {
              e.printStackTrace();
            }
          }}).run();

          new Thread(new Runnable() { public void run() {
            BufferedReader in = new BufferedReader(new InputStreamReader(spark.getInputStream()));
            String line;
            try {
              while ((line = in.readLine()) != null) {
                System.out.println(line);
              }
            } catch (IOException e) {
              e.printStackTrace();
            }
          }}).run();

          System.out.println("Waiting for job "+i+" to finish...");

          int exitCode = 0;
          try {
            exitCode = spark.waitFor();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

          System.out.println("Finished job "+i+"! Exit code:" + exitCode);
          return null;
        }
      });
    }
    // Start thread 2
    /*Future<Long> future2 = executorService.submit(new Callable<Long>() {
      @Override
      public Long call() throws Exception {
        JavaRDD<String> file2 = sc.textFile("/path/to/test_doc2");
        return file2.count();
      }
    });*/
    //String query = args[2];

  }

  public static class InputStreamReaderRunnable implements Runnable {

    private BufferedReader reader;

    private String name;

    public InputStreamReaderRunnable(InputStream is, String name) {
      this.reader = new BufferedReader(new InputStreamReader(is));
      this.name = name;
    }

    public void run() {
      System.out.println("InputStream " + name + ":");
      try {
        String line = reader.readLine();
        while (line != null) {
          System.out.println(line);
          line = reader.readLine();
        }
        reader.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

}