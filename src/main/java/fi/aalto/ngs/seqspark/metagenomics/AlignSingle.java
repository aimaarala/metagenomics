package fi.aalto.ngs.seqspark.metagenomics;/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import htsjdk.samtools.*;
import org.apache.commons.cli.*;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**Usage
 *  spark-submit --master local[4] --driver-memory 4g --executor-memory 4g --class fi.aalto.ngs.seqspark.metagenomics.ParallelBWAMemPiped target/metagenomics-0.9-jar-with-dependencies.jar -fastqdir /user/root/fqsplits -ref /media/data/genomes/hg38/hg38.fa -bwaout /user/root/bwaout -align
 spark-submit --master yarn-cluster --driver-cores 5 --conf spark.yarn.executor.memoryOverhead=3000 --conf spark.task.maxFailures=40 --conf spark.yarn.max.executor.failures=1000 --num-executors 10 --executor-memory 3g --conf spark.hadoop.validateOutputSpecs=true --class fi.aalto.ngs.seqspark.metagenomics.Piped target/metagenomics-0.9-jar-with-dependencies.jar -fastqdir /user/root/fqsplits -ref /index/hg38.fa -bwaout /user/hdfs/bwaout -align

 **/


public class AlignSingle {

  //Example run from examples/target dir
  //su hdfs
  //unset SPARK_JAVA_OPTS

  private static String alignmentTable = "alignedreads";
  private static String rawreadsTable = "rawreads";


  public static void main(String[] args) throws IOException {
    SparkConf conf = new SparkConf().setAppName("AlignSingle");
    JavaSparkContext sc = new JavaSparkContext(conf);
    SQLContext sqlContext = new HiveContext(sc.sc());

    //String query = args[2];

    Options options = new Options();
    Option splitDirOpt = new Option( "fastq", true, "Path to output directory in hdfs." );
    Option refOpt = new Option( "ref", true, "Path to fasta reference file." );
    Option opOpt = new Option( "out", true, "HDFS path for output files. If not present, the output files are not moved to HDFS." );
    Option pairedOpt = new Option( "paired", "Use paired end reads" );
    Option samOpt = new Option( "samopts", true, "Options for samtools view given within parenthesis e.g.'-F 4'" );

    options.addOption( refOpt );
    options.addOption( opOpt );
    options.addOption( splitDirOpt );
    options.addOption( pairedOpt );
    options.addOption( samOpt );
    options.addOption( new Option( "unmapped", "keep unmapped, true or false. If not any given, all alignments/reads are persisted" ) );
    options.addOption( new Option( "mapped", "keep mapped, true or false. If not any given, all alignments/reads are persisted" ) );
    options.addOption( new Option( "format", true, "bam or sam, fastq is default" ));
    options.addOption(new Option( "interleaved", "Interleaved read input" ));

    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "spark-submit <spark specific args>", options, true );

    CommandLineParser parser = new BasicParser();
    CommandLine cmd = null;
    try {
      // parse the command line arguments
      cmd = parser.parse( options, args );

    }
    catch( ParseException exp ) {
      // oops, something went wrong
      System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
    }
    String splitDir = (cmd.hasOption("fastq")==true)? cmd.getOptionValue("fastq"):null;
    String ref = (cmd.hasOption("ref")==true)? cmd.getOptionValue("ref"):null;
    String bwaOutDir = (cmd.hasOption("out")==true)? cmd.getOptionValue("out"):null;
    boolean paired = cmd.hasOption("paired");
    String samtoolopts = (cmd.hasOption("samopts")==true)? cmd.getOptionValue("samopts"):"";
    boolean unmapped = cmd.hasOption("unmapped");
    boolean mapped = cmd.hasOption("mapped");
    boolean interleaved = cmd.hasOption("mapped");
    String format = (cmd.hasOption("format") == true) ? cmd.getOptionValue("format") : "fastq";

    final String finalSplitDir = splitDir;
    final String finalBwaOutDir = bwaOutDir;

    String splitDir2 = null;
    if(paired)
      splitDir2 = splitDir+"2";
    final String finalSplitDir2 = splitDir2;

    //TODO: handle compressed files, use LZO if possible as it is splittable format
   /* FileStatus[] st2 = fs.listStatus(new Path(splitDir2));
    ArrayList<Tuple2<String,String>> splitFileList = new ArrayList<>();
    for (int i=0;i<st.length;i++){
      splitFileList.add(new Tuple2<String, String>(st[i].getPath().getName().toString(), st2[i].getPath().getName().toString()));
    }
    JavaRDD<Tuple2<String, String>> splitFilesRDD = sc.parallelize(splitFileList);
    */

    FileSystem fs = FileSystem.get(sc.hadoopConfiguration());
    FileStatus[] st = fs.listStatus(new Path(splitDir));
    ArrayList<String> splitFileList = new ArrayList<>();
    for (int i=0;i<st.length;i++){
      splitFileList.add(st[i].getPath().getName().toString());
    }
    JavaRDD<String> splitFilesRDD = sc.parallelize(splitFileList);

    //conf.set("spark.default.parallelism", String.valueOf(splitFileList.size()/2));

    //Do bwa alignment if align option is given

      JavaRDD<String> rdd = splitFilesRDD.mapPartitions(file -> {
        String fname = file.next();
        //System.out.println(fname);
          Process process;
          if(paired) {
            //| samtools view "+samtoolopts+"
            //bwa mem /tmp/adhoc.fa <(hdfs dfs -text /user/root/fqsplits/split_1.fq) <(hdfs dfs -text /user/root/fqsplits2/split_1.fq) | hdfs dfs -put /dev/stdin /user/root/bwaout/split_1.fq.sam
            String command = "bwa mem " + ref + " <(hdfs dfs -text "+ finalSplitDir +"/"+fname+") <(hdfs dfs -text "+ finalSplitDir2 +"/"+fname+") | hdfs dfs -put /dev/stdin " + finalBwaOutDir + "/" + fname + ".sam ";
            ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", command);
            System.out.println(command);
            process = pb.start();
          }else{
            //String command = "bwa mem " + ref + " <(hdfs dfs -text "+splitDir+"/"+fname+") | hdfs dfs -put /dev/stdin " + bwaOutDir + "/" + fname + ".sam";  | samtools view -f 4 | hdfs dfs -put /dev/stdin " + finalBwaOutDir + "/" + fname + ".sam"
            //ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", "hdfs dfs -text "+ finalSplitDir +"/"+fname+" | bwa mem -p " + ref + " /dev/stdin");

            String command = "hdfs dfs -text "+ finalSplitDir +"/"+fname+" | bwa mem"+ ((interleaved == true) ? " -p" : "") +" " + ref + " /dev/stdin";
            //this is right way, Runtime.getRuntime().exec(command) does not work as InputStream is not piped
            ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", command);
            System.out.println(command);
            process = pb.start();
          }

        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        ArrayList<String> out = new ArrayList<String>();
        while ((line = br.readLine()) != null) {
          //String flag = fields[1];
          SAMRecord record;
          //We want only mapped reads
          if(unmapped==true){
            final SAMLineParser samLP = new SAMLineParser(new DefaultSAMRecordFactory(), ValidationStringency.SILENT,  new SAMFileHeader(), null, null);
            try{
              record = samLP.parseLine(line);
              if(record.getReadUnmappedFlag()){
                out.add(line);
              }
            }catch (SAMFormatException e){
              System.out.println(e);
            }
          }
          else if(mapped==true){
            final SAMLineParser samLP = new SAMLineParser(new DefaultSAMRecordFactory(), ValidationStringency.SILENT,  new SAMFileHeader(), null, null);
            try{
              record = samLP.parseLine(line);
              if(!record.getReadUnmappedFlag()){
                out.add(line);
              }
            }catch (SAMFormatException e){
              System.out.println(e);
            }
          }
          else{  //keep all
            out.add(line);
          }

        }
        process.waitFor();
        br.close();
        return out.iterator();
        });

    if(format!=null)
      new HDFSWriter(rdd, finalBwaOutDir, format, sc);
    else
      rdd.saveAsTextFile(finalBwaOutDir);

    sc.stop();

  }

}