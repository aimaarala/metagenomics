package fi.aalto.ngs.seqspark.metagenomics;


import com.github.lindenb.jbwa.jni.BwaIndex;
import com.github.lindenb.jbwa.jni.BwaMem;
import com.github.lindenb.jbwa.jni.ShortRead;
import org.apache.commons.cli.*;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.seqdoop.hadoop_bam.FastqInputFormat;
import org.seqdoop.hadoop_bam.FastqOutputFormat;
import org.seqdoop.hadoop_bam.SequencedFragment;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**Usage
 *  Usage example:
 spark-submit --master local[4] --class fi.aalto.ngs.seqspark.metagenomics.FilterNAlign target/metagenomics-0.9-jar-with-dependencies.jar -fastq fqchunks -out flt -ref /index/adhoc.fa -format fastq -unmapped

 spark-submit --master yarn --deploy-mode client --conf spark.dynamicAllocation.enabled=true --executor-memory 15g  --conf spark.yarn.executor.memoryOverhead=5000 --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --class fi.aalto.ngs.seqspark.metagenomics.FilterNAlign target/metagenomics-0.9-jar-with-dependencies.jar -fastq interleaved -out alignedfiltered -ref /index/hg38.fa -format fastq -unmapped

 NOTE: DO NOT PUT INDEX FILES UNDER ROOT FOLDER, or YARN can not access those
 **/


public class AlignInterleaved {

  //Example run from examples/target dir
  //su hdfs
  //unset SPARK_JAVA_OPTS

  public static void main(String[] args) throws IOException {
    SparkConf conf = new SparkConf().setAppName("AlignInterleaved");
    JavaSparkContext sc = new JavaSparkContext(conf);

    //String query = args[2];

    Options options = new Options();
    Option pathOpt = new Option( "in", true, "Path to fastq file in hdfs." );    //gmOpt.setRequired(true);
    Option refOpt = new Option( "ref", true, "Path to fasta reference file in local FS." );
    Option fqoutOpt = new Option( "out", true, "" );
    //Option formatOpt = new Option( "format", true, "bam or sam, fastq is default" );

    options.addOption( pathOpt );
    options.addOption( refOpt );
    options.addOption( fqoutOpt );
    options.addOption(new Option( "partitions", true, "number of file partitions to save, defaults to same as number of input partitions"));
    options.addOption(new Option( "minsize", true, "minsize for partition (in bytes), defaults to 0"));

    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "spark-submit <spark specific args>", options, true );

    CommandLineParser parser = new BasicParser();
    CommandLine cmd = null;
    try {
      // parse the command line arguments
      cmd = parser.parse( options, args );

    }
    catch( ParseException exp ) {
      // oops, something went wrong
      System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
    }
    String input = cmd.getOptionValue("in");
    String ref = (cmd.hasOption("ref")==true)? cmd.getOptionValue("ref"):null;
    String outDir = (cmd.hasOption("out")==true)? cmd.getOptionValue("out"):null;
    int numpartitions = (cmd.hasOption("partitions")==true)? Integer.valueOf(cmd.getOptionValue("partitions")):0;
    int minsize = (cmd.hasOption("minsize")==true)? Integer.valueOf(cmd.getOptionValue("minsize")):0;

    //String format = (cmd.hasOption("format") == true) ? cmd.getOptionValue("format") : "bam";
    sc.hadoopConfiguration().setInt("mapreduce.input.fileinputformat.split.minsize", minsize);
    /*config.setInt("mapred.max.split.size", splits);
        config.setInt("mapreduce.input.fileinputformat.split.minsize", splits);*/
    //conf.set("spark.default.parallelism", String.valueOf(splits));

    JavaPairRDD<Text, SequencedFragment> fastqRDD = sc.newAPIHadoopFile(input, FastqInputFormat.class, Text.class, SequencedFragment.class, sc.hadoopConfiguration());

    JavaPairRDD<Text, SequencedFragment> alignmentRDD = fastqRDD.mapPartitionsToPair(split -> {
      //THIS MUST BE LOADED HERE FOR YARN
      System.loadLibrary("bwajni");
      //TODO: Modify JBWA to use SAMRecord class, this woudl radically reduce map operations
      BwaIndex index = new BwaIndex(new File(ref));
      BwaMem mem = new BwaMem(index);

      List<ShortRead> L1 = new ArrayList<ShortRead>();
      List<ShortRead> L2 = new ArrayList<ShortRead>();

      //BUG OR FEATURE? org.apache.hadoop.io.Text.getBytes() gives different than Text.toString().getBytes(). USE Text.toString().getBytes()!!
      while (split.hasNext()) {
        Tuple2<Text, SequencedFragment> next = split.next();
        String key = next._1.toString();
        //FIXME: With BWA 0.7.12 new Illumina reads fail: [mem_sam_pe] paired reads have different names: "M02086:39:AA7PR:1:1101:6095:20076 1:N:0:1", "M02086:39:AA7PR:1:1101:6095:20076 2:N:0:1"
        String[] keysplit = key.split(" ");
        key = keysplit[0];

        SequencedFragment sf = new SequencedFragment();
        sf.setQuality(new Text(next._2.getQuality().toString()));
        sf.setSequence(new Text(next._2.getSequence().toString()));

        if (split.hasNext()) {

          Tuple2<Text, SequencedFragment> next2 = split.next();
          String key2 = next2._1.toString();
          String[] keysplit2 = key2.split(" ");
          key2 = keysplit2[0];
          //key2 = key2.replace(" 2:N:0:1","/2");

          SequencedFragment sf2 = new SequencedFragment();
          sf2.setQuality(new Text(next2._2.getQuality().toString()));
          sf2.setSequence(new Text(next2._2.getSequence().toString()));

          if(key.equalsIgnoreCase(key2)){
            L1.add(new ShortRead(key, sf.getSequence().toString().getBytes(), sf.getQuality().toString().getBytes()));
            L2.add(new ShortRead(key2, sf2.getSequence().toString().getBytes(), sf2.getQuality().toString().getBytes()));
          }else
            split.next();
        }
      }

      String[] aligns = mem.align(L1, L2);

      if (aligns != null) {

        ArrayList<Tuple2<Text, SequencedFragment>> filtered = new ArrayList<Tuple2<Text, SequencedFragment>>();
        Arrays.asList(aligns).forEach(aln -> {
          //String ali = aln.replace("\r\n", "").replace("\n", "").replace(System.lineSeparator(), "");
          String[] fields = aln.split("\\t");

          int flag = Integer.parseInt(fields[1]);

          if (flag == 77) {
            String name = fields[0] + "/1";
            String bases = fields[9];
            String quality = fields[10];

            Text t = new Text(name);
            SequencedFragment sf = new SequencedFragment();
            sf.setSequence(new Text(bases));
            sf.setQuality(new Text(quality));
            filtered.add(new Tuple2<Text, SequencedFragment>(t, sf));
          } else if (flag == 141) {
            String name = fields[0] + "/2";
            String bases = fields[9];
            String quality = fields[10];

            Text t = new Text(name);
            SequencedFragment sf = new SequencedFragment();
            sf.setSequence(new Text(bases));
            sf.setQuality(new Text(quality));
            filtered.add(new Tuple2<Text, SequencedFragment>(t, sf));

          }

        });
        return filtered.iterator();
        //return Arrays.asList(aligns);
      } else
        return new ArrayList<Tuple2<Text, SequencedFragment>>().iterator(); //NULL ALIGNMENTS

    });

    //TODO: optionally group by sample id and save to different locations, Sample Id must be stored into read name value in the interleaving phase

    if(numpartitions!=0){
      //alignmentRDD.count();
      alignmentRDD.coalesce(numpartitions).saveAsNewAPIHadoopFile(outDir, Text.class, SequencedFragment.class, FastqOutputFormat.class, sc.hadoopConfiguration());
    }
    else
      alignmentRDD.saveAsNewAPIHadoopFile(outDir, Text.class, SequencedFragment.class, FastqOutputFormat.class, sc.hadoopConfiguration());

    sc.stop();

  }

}