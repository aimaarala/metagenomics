package fi.aalto.ngs.seqspark.metagenomics;/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.commons.cli.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**Usage
 * UNSET SPARK_JAVA_OPTS with YARN!
  Depending on the number of partitions, this gives more output than if reads are processed sequentially.
  Closely similar output to sequential can be obtained by tuning input parameters
 Took only 10s with 15 cores 5G ram in each per sequenced individual (4G unmaapped reads)
 Run local:
 > spark-submit  --master local[15] --executor-memory 5g --class fi.aalto.ngs.seqspark.metagenomics.normalization.NormalizeReads metagenomics-0.9-jar-with-dependencies.jar -fastq fqsplits -out normalized

 YARN needs FULL PATH to files
 Do not use dynamic allocation with this
  --conf spark.dynamicAllocation.enabled=false
 Be sure that there is enough available cores and memory for each partition!
 See for tuning http://blog.cloudera.com/blog/2015/03/how-to-tune-your-apache-spark-jobs-part-2/

 Run on Yarn cluster:

  > spark-submit --master yarn --deploy-mode client --conf spark.dynamicAllocation.enabled=true --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --class fi.aalto.ngs.seqspark.metagenomics.NormalizeParts metagenomics-0.9-jar-with-dependencies.jar -in /user/root/temp/1/out -temp /user/root/t -out /user/root/normalized -samplenum 1 -repartition 1 -p
 **/


public class NormalizeParts {

  //Example run from examples/target dir
  //su hdfs
  //unset SPARK_JAVA_OPTS

  public static void main(String[] args) throws IOException {
    SparkConf conf = new SparkConf().setAppName("NormalizeParts");
    JavaSparkContext sc = new JavaSparkContext(conf);

    //String query = args[2];
    //System.setProperty("SPARK_JAVA_OPTS", null);

    Options options = new Options();

    //gmOpt.setRequired(true);

    Option pairedOpt = new Option( "paired", "Use paired end reads" );
    Option splitOpt = new Option( "in", true, "" );
    //-C 15 -k 19 -N 4 -x 1e+8
    Option cOpt = new Option( "C", true, "Coverage threshold" );
    Option kOpt = new Option( "k", true, "k-mer size" );
    Option nOpt = new Option( "N", true, "" );
    Option xOpt = new Option( "x", true, "" );
    Option pOpt = new Option( "p", "interleaved option, if normalization fails, its probably that some read is shorter than k, then use smaller k, see:https://github.com/dib-lab/khmer/issues/1338" );
    Option ouOpt = new Option( "out", true, "" );
    options.addOption(new Option( "dw", "Write directly to hdfs without Spark partitioning" ));
    options.addOption(new Option( "repartition", true,  "number of partitions" ));
    options.addOption(new Option( "samplenum", true, "index of sample sequence"));
    options.addOption(new Option( "temp", true, "Absolute path to temp dir"));

    options.addOption( pairedOpt );
    options.addOption( splitOpt );
    options.addOption( cOpt );
    options.addOption( nOpt );
    options.addOption( kOpt );
    options.addOption( xOpt );
    options.addOption( pOpt );
    options.addOption( ouOpt );
    options.addOption( new Option( "debug", ""));

    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "spark-submit <spark specific args>", options, true );

    CommandLineParser parser = new BasicParser();
    CommandLine cmd = null;
    try {
      // parse the command line arguments
      cmd = parser.parse( options, args );

    }
    catch( ParseException exp ) {
      // oops, something went wrong
      System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
    }
    String splitDir = (cmd.hasOption("in")==true)? cmd.getOptionValue("in"):null;
    String outDir = (cmd.hasOption("out")==true)? cmd.getOptionValue("out"):null;
    int numpartitions = (cmd.hasOption("repartition")==true)? Integer.valueOf(cmd.getOptionValue("repartition")):0;
    String tempdir = (cmd.hasOption("temp")==true)? cmd.getOptionValue("temp"):null;
    String samplenum = cmd.getOptionValue("samplenum");
    boolean debug = (cmd.hasOption("debug")==true);

    //boolean paired = cmd.hasOption("paired");
    int c = (cmd.hasOption("C")==true)? Integer.valueOf(cmd.getOptionValue("C")):15;
    int k = (cmd.hasOption("k")==true)? Integer.valueOf(cmd.getOptionValue("k")):16;
    int N = (cmd.hasOption("N")==true)? Integer.valueOf(cmd.getOptionValue("N")):4;
    int x = (cmd.hasOption("x")==true)? Integer.valueOf(cmd.getOptionValue("x")):100000000;

    boolean p = cmd.hasOption("p");
    boolean dw = cmd.hasOption("dw");


    //TODO: MAKE SURE  THAT READS are LONGER than value K!! should be filtered or shortest length reported in alignment phase!
    if(numpartitions!=0){
      sc.textFile(splitDir).coalesce(numpartitions).saveAsTextFile(tempdir+"/"+samplenum);
      splitDir=tempdir+"/"+samplenum;
    }

    final String finalSplitDir = splitDir;
    FileSystem fs = FileSystem.get(new Configuration());

    FileStatus[] st = fs.listStatus(new Path(splitDir));
    ArrayList<String> splitFileList = new ArrayList<>();
    for (int i=0;i<st.length;i++){
      String fn = st[i].getPath().getName().toString();
      if(!fn.equalsIgnoreCase("_SUCCESS"))
        splitFileList.add(fn);
    }
    JavaRDD<String> splitFilesRDD = sc.parallelize(splitFileList, splitFileList.size());

    //mapPartitions so that read structure remains when partition changes

    if(dw) {
      splitFilesRDD.foreachPartition(f -> {
        String fname = f.next();
        //hdfs dfs -text /user/root/ramdisk/bwa/splits/split_0.fq | normalize-by-median.py -C 15 -k 19 -N 4 -x 1e+8 -o <(hdfs dfs -put /dev/stdin /user/root/normalized/normalized_split_0.fq)
        //TODO: If yarn used, put directly to HDFS
        String normalize_cmd = "/srv/hops/hadoop/bin/hdfs dfs -text " + finalSplitDir + "/" + fname + " | /srv/non_hdfs/diginorm/khmer/scripts/normalize-by-median.py " + ((p == true) ? "-p " : "") + "-C" + c + " -k" + k + " -N" + N + " -x" + x + " /dev/stdin -o /dev/stdout | hdfs dfs -put /dev/stdin "+outDir+"/"+fname;
        System.out.println(normalize_cmd);
        //this is right way, Runtime.getRuntime().exec(command) does not work as InputStream is not piped
        ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", normalize_cmd);
        Process process = pb.start();
        process.waitFor();

      });
    }
  else{
      JavaRDD<String> outRDD = splitFilesRDD.mapPartitions(f -> {
        Process process;
        String fname = f.next();
        String normalize_cmd = "/srv/hops/hadoop/bin/hdfs dfs -text " + finalSplitDir + "/" + fname + " | /srv/non_hdfs/diginorm/khmer/scripts/normalize-by-median.py " + ((p == true) ? "-p " : "") + "-C" + c + " -k" + k + " -N" + N + " -x" + x + " /dev/stdin -o "+tempdir+ "/" + fname;

        System.out.println(normalize_cmd);
        //this is right way, Runtime.getRuntime().exec(command) does not work as InputStream is not piped
        ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", normalize_cmd);
        process = pb.start();

        if(debug){
          ArrayList<String> errout = new ArrayList<String>();
          BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
          String e;
          while ((e = err.readLine()) != null) {
            System.out.println(e);
            errout.add(e);
          }
          process.waitFor();
          err.close();
          return errout.iterator();
        }
      else{
          BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
          String line;
          ArrayList<String> out = new ArrayList<String>();
          while ((line = in.readLine()) != null) {
            //System.out.println(line);
            out.add(line);
          }
          process.waitFor();
          in.close();
          return out.iterator();
        }

      });

      outRDD.saveAsTextFile(outDir);

    }

    //TODO: convert to fastq rdds before writing.. no need if data already partitioned so that read does not break
    /*if(format!=null)
      new HDFSWriter(outRDD, outDir, format, sc);
    else*/

    if(numpartitions!=0)
      fs.delete(new Path(splitDir), true);
    sc.stop();

  }

}