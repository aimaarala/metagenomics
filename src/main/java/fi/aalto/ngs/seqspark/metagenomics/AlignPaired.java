package fi.aalto.ngs.seqspark.metagenomics;


import com.github.lindenb.jbwa.jni.BwaIndex;
import com.github.lindenb.jbwa.jni.BwaMem;
import com.github.lindenb.jbwa.jni.ShortRead;
import org.apache.commons.cli.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.NLineInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.seqdoop.hadoop_bam.FastqInputFormat.FastqRecordReader;
import org.seqdoop.hadoop_bam.FastqOutputFormat;
import org.seqdoop.hadoop_bam.SequencedFragment;
import scala.Tuple2;

import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**Usage
 *  Usage example:
 spark-submit --master local[4] --class fi.aalto.ngs.seqspark.metagenomics.FilterNAlign target/metagenomics-0.9-jar-with-dependencies.jar -fastq fqchunks -out flt -ref /index/adhoc.fa -format fastq -unmapped

 spark-submit --master yarn --deploy-mode client --conf spark.dynamicAllocation.enabled=true --executor-memory 15g  --conf spark.yarn.executor.memoryOverhead=5000 --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --class fi.aalto.ngs.seqspark.metagenomics.FilterNAlign target/metagenomics-0.9-jar-with-dependencies.jar -fastq interleaved -out alignedfiltered -ref /index/hg38.fa -format fastq -unmapped

 NOTE: DO NOT PUT INDEX FILES UNDER ROOT FOLDER, or YARN can not access those
 **/


public class AlignPaired {

  //Example run from examples/target dir
  //su hdfs
  //unset SPARK_JAVA_OPTS
  public static void main(String[] args) throws IOException {
    SparkConf conf = new SparkConf().setAppName("AlignPaired");
    //conf.set("spark.scheduler.mode", "FAIR");
    //conf.set("spark.scheduler.allocation.file", "/opt/cloudera/parcels/CDH-5.10.0-1.cdh5.10.0.p0.41/etc/hadoop/conf.dist/pools.xml");
    JavaSparkContext sc = new JavaSparkContext(conf);
    //sc.setLocalProperty("spark.scheduler.pool", "production");

    //String query = args[2];

    Options options = new Options();
    Option pathOpt = new Option( "fastq", true, "Path to fastq file in hdfs." );
    Option path2Opt = new Option( "fastq2", true, "Path to fastq file in hdfs." );
    //gmOpt.setRequired(true);
    Option refOpt = new Option( "ref", true, "Path to fasta reference file." );
    Option fqoutOpt = new Option( "out", true, "" );

    //-C 15 -k 19 -N 4 -x 1e+8
    Option cOpt = new Option( "C", true, "Coverage threshold" );
    Option kOpt = new Option( "k", true, "k-mer size" );
    Option nOpt = new Option( "N", true, "" );
    Option xOpt = new Option( "x", true, "" );
    Option pOpt = new Option( "p", "interleaved read normalization" );
    //Option formatOpt = new Option( "format", true, "bam or sam, fastq is default" );

    options.addOption( pathOpt );
    options.addOption( path2Opt );
    options.addOption( refOpt );
    options.addOption( fqoutOpt );

    options.addOption( cOpt );
    options.addOption( nOpt );
    options.addOption( kOpt );
    options.addOption( xOpt );
    options.addOption( pOpt );
    options.addOption(new Option( "outpartitions", true, "number of file partitions to save, default is the number of input partitions"));
    options.addOption(new Option( "partitionsize", true, "Number of reads in partition, depends on the size of read file, number of cores and available memory."));
    options.addOption(new Option( "samplenum", true, "index of sample sequence"));
    options.addOption(new Option( "temp", true, "Absolute path to temp dir"));
    options.addOption(new Option( "decompress", "decompress fastq files and save to temp"));
    options.addOption(new Option( "bitflag", "use flag bit to determine unmapepd reads, does not write paired end information to the name"));

    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "spark-submit <spark specific args>", options, true );

    CommandLineParser parser = new BasicParser();
    CommandLine cmd = null;
    try {
      // parse the command line arguments
      cmd = parser.parse( options, args );

    }
    catch( ParseException exp ) {
      // oops, something went wrong
      System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
    }
    String fastq = cmd.getOptionValue("fastq");
    String fastq2 = (cmd.hasOption("fastq2")==true)? cmd.getOptionValue("fastq2"):null;
    String ref = (cmd.hasOption("ref")==true)? cmd.getOptionValue("ref"):null;
    String outDir = (cmd.hasOption("out")==true)? cmd.getOptionValue("out"):null;
    String tempdir = (cmd.hasOption("temp")==true)? cmd.getOptionValue("temp"):null;
    String samplenum = cmd.getOptionValue("samplenum");
    boolean bitflag = cmd.hasOption("bitflag");
    int numpartitions = (cmd.hasOption("outpartitions")==true)? Integer.valueOf(cmd.getOptionValue("outpartitions")):0;
    int splitsize = (cmd.hasOption("partitionsize")==true)? Integer.parseInt(cmd.getOptionValue("partitionsize")):0;
    boolean decompress = cmd.hasOption("decompress");
    FileSystem fs = FileSystem.get(new Configuration());

    if(decompress){
      decompress(fs, fastq, tempdir+"/"+samplenum+"/forward.fq");
      decompress(fs, fastq2, tempdir+"/"+samplenum+"/reverse.fq");

      fastq = tempdir+"/"+samplenum+"/forward.fq";
      fastq2 = tempdir+"/"+samplenum+"/reverse.fq";
    }

    int splitlen = splitsize*4; //reads are expressed by 4 lines

    FileStatus fst = fs.getFileStatus(new Path(fastq));
    FileStatus fst2 = fs.getFileStatus(new Path(fastq2));

    List<FileSplit> nlif = NLineInputFormat.getSplitsForFile(fst, sc.hadoopConfiguration(), splitlen);
    List<FileSplit> nlif2 = NLineInputFormat.getSplitsForFile(fst2, sc.hadoopConfiguration(), splitlen);

    JavaRDD<FileSplit> splitRDD = sc.parallelize(nlif, nlif.size());
    JavaRDD<FileSplit> splitRDD2 = sc.parallelize(nlif2, nlif2.size());
    JavaPairRDD<FileSplit, FileSplit> zips = splitRDD.zip(splitRDD2);
    //SizeEstimator.estimate(alignmentRDD)
    JavaPairRDD<Text, SequencedFragment> alignmentRDD = zips.mapPartitionsToPair( splits ->  {
      Tuple2<FileSplit, FileSplit> split = splits.next();
      FastqRecordReader fqreader = new FastqRecordReader(new Configuration(), split._1);
      FastqRecordReader fqreader2 = new FastqRecordReader(new Configuration(), split._2);

      //THIS MUST BE LOADED HERE IN YARN
      System.loadLibrary("bwajni");
      //TODO: Modify JBWA to use SAMRecord class, this woudl radically reduce map operations
      BwaIndex index = new BwaIndex(new File(ref));
      BwaMem mem = new BwaMem(index);

      List<ShortRead> L1 = new ArrayList<ShortRead>();
      List<ShortRead> L2 = new ArrayList<ShortRead>();

      //BUG OR FEATURE? org.apache.hadoop.io.Text.getBytes() gives different than Text.toString().getBytes(). USE Text.toString().getBytes()!!
      while (fqreader.nextKeyValue()) {
        String key = fqreader.getCurrentKey().toString();
        //FIXME: With BWA 0.7.12 new Illumina reads fail: [mem_sam_pe] paired reads have different names: "M02086:39:AA7PR:1:1101:6095:20076 1:N:0:1", "M02086:39:AA7PR:1:1101:6095:20076 2:N:0:1"
        String[] keysplit = key.split(" ");
        key = keysplit[0];

        SequencedFragment sf = new SequencedFragment();
        sf.setQuality(new Text(fqreader.getCurrentValue().getQuality().toString()));
        sf.setSequence(new Text(fqreader.getCurrentValue().getSequence().toString()));

        if (fqreader2.nextKeyValue()) {

          String key2 = fqreader2.getCurrentKey().toString();
          String[] keysplit2 = key2.split(" ");
          key2 = keysplit2[0];
          //key2 = key2.replace(" 2:N:0:1","/2");

          SequencedFragment sf2 = new SequencedFragment();
          sf2.setQuality(new Text(fqreader2.getCurrentValue().getQuality().toString()));
          sf2.setSequence(new Text(fqreader2.getCurrentValue().getSequence().toString()));

          if(key.equalsIgnoreCase(key2)){
            L1.add(new ShortRead(key, sf.getSequence().toString().getBytes(), sf.getQuality().toString().getBytes()));
            L2.add(new ShortRead(key2, sf2.getSequence().toString().getBytes(), sf2.getQuality().toString().getBytes()));
          }//else split.next();
        }
      }

      String[] aligns = mem.align(L1, L2);

      if (aligns != null) {

        ArrayList<Tuple2<Text, SequencedFragment>> filtered = new ArrayList<Tuple2<Text, SequencedFragment>>();
        Arrays.asList(aligns).forEach(aln -> {
          //String ali = aln.replace("\r\n", "").replace("\n", "").replace(System.lineSeparator(), "");
          String[] fields = aln.split("\\t");

          int flag = Integer.parseInt(fields[1]);

          if(bitflag){
            if (BigInteger.valueOf(flag).testBit(4)) {
              String name = fields[0];
              String bases = fields[9];
              String quality = fields[10];

              Text t = new Text(name);
              SequencedFragment sf = new SequencedFragment();
              sf.setSequence(new Text(bases));
              sf.setQuality(new Text(quality));
              filtered.add(new Tuple2<Text, SequencedFragment>(t, sf));
            }
          }else{
            if (flag == 77) {
              String name = fields[0] + "/1";
              String bases = fields[9];
              String quality = fields[10];

              Text t = new Text(name);
              SequencedFragment sf = new SequencedFragment();
              sf.setSequence(new Text(bases));
              sf.setQuality(new Text(quality));
              filtered.add(new Tuple2<Text, SequencedFragment>(t, sf));
            } else if (Integer.parseInt(fields[1]) == 141) {
              String name = fields[0] + "/2";
              String bases = fields[9];
              String quality = fields[10];

              Text t = new Text(name);
              SequencedFragment sf = new SequencedFragment();
              sf.setSequence(new Text(bases));
              sf.setQuality(new Text(quality));
              filtered.add(new Tuple2<Text, SequencedFragment>(t, sf));

            }
          }

        });
        return filtered.iterator();
        //return Arrays.asList(aligns);
      } else
        return new ArrayList<Tuple2<Text, SequencedFragment>>().iterator(); //NULL ALIGNMENTS

    });

    if(numpartitions!=0){
      alignmentRDD.count();
      //sc.getConf().set("spark.executor.memory", "25g");
      alignmentRDD.coalesce(numpartitions).saveAsNewAPIHadoopFile(outDir+"/"+samplenum, Text.class, SequencedFragment.class, FastqOutputFormat.class, sc.hadoopConfiguration());
    }else
      alignmentRDD.saveAsNewAPIHadoopFile(outDir+"/"+samplenum, Text.class, SequencedFragment.class, FastqOutputFormat.class, sc.hadoopConfiguration());

    if(decompress)
      fs.delete(new Path(tempdir+"/"+samplenum), true);

    sc.stop();

  }

  private static void decompress(FileSystem fs, String in, String outpath) throws IOException {
    //TODO: handle compressed files, use LZO if possible as it is splittable format
    Configuration conf = new Configuration();
    CompressionCodecFactory factory = new CompressionCodecFactory(conf);
    // the correct codec will be discovered by the extension of the file

    CompressionCodec codec = factory.getCodec(new Path(in));
    //Decompressing zip file.
    InputStream is = codec.createInputStream(fs.open(new Path(in)));
    OutputStream out = fs.create(new Path(outpath));
    //Write decompressed out
    IOUtils.copyBytes(is, out, conf);
    is.close();
    out.close();
  }

}