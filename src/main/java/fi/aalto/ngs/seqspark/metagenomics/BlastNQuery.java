package fi.aalto.ngs.seqspark.metagenomics;

import org.apache.commons.cli.*;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by ilamaa on 2017-04-14.
 */
public class BlastNQuery {

    public static void main(String[] args) throws IOException {


        Options options = new Options();

        options.addOption(new Option( "out", true, "Input HDFS directory (should have multiple subdirectories)" ));
        options.addOption(new Option( "in", true, "" ));
        options.addOption(new Option( "partitions", true, "Number of partitions" ));

        options.addOption(new Option( "word_size", ""));
        options.addOption(new Option( "gapopen", true, "" ));
        options.addOption(new Option( "gapextend", true, "" ));
        options.addOption(new Option( "penalty", true, "" ));
        options.addOption(new Option( "reward", true, "" ));
        options.addOption(new Option( "max_target_seqs", true, "" ));
        options.addOption(new Option( "evalue", true, "" ));
        options.addOption(new Option( "show_gis", "" ));
        options.addOption(new Option( "outfmt", true, "" ));
        options.addOption(new Option( "db", true, "" ));
        options.addOption(new Option( "task", true, "" ));
        options.addOption(new Option( "fa", "Take only " ));

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "spark-submit <spark specific args>", options, true );

        CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;
        try {
            // parse the command line arguments
            cmd = parser.parse( options, args );
        }
        catch( ParseException exp ) {
            // oops, something went wrong
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
            System.exit(1);
        }

        SparkConf conf = new SparkConf().setAppName("BlastNQuery");
        JavaSparkContext sc = new JavaSparkContext(conf);

        int word_size = (cmd.hasOption("word_size")==true)? Integer.valueOf(cmd.getOptionValue("word_size")):11;
        int gapopen = (cmd.hasOption("gapopen")==true)? Integer.valueOf(cmd.getOptionValue("gapopen")):0;
        int gapextend = (cmd.hasOption("gapextend")==true)? Integer.valueOf(cmd.getOptionValue("gapextend")):2;
        int penalty = (cmd.hasOption("penalty")==true)? Integer.valueOf(cmd.getOptionValue("penalty")):-1;
        int reward = (cmd.hasOption("reward")==true)? Integer.valueOf(cmd.getOptionValue("reward")):1;
        int max_target_seqs = (cmd.hasOption("max_target_seqs")==true)? Integer.valueOf(cmd.getOptionValue("max_target_seqs")):10;
        double evalue = (cmd.hasOption("evalue")==true)? Double.valueOf(cmd.getOptionValue("evalue")):0.001;
        boolean show_gis = cmd.hasOption("show_gis");
        int outfmt = (cmd.hasOption("outfmt")==true)? Integer.valueOf(cmd.getOptionValue("outfmt")):6;
        String db = (cmd.hasOption("db")==true)? cmd.getOptionValue("db"):"/mnt/hdfs/1/Index_blastn/nt";
        String input = cmd.getOptionValue("in");
        String output = cmd.getOptionValue("out");
        String task = (cmd.hasOption("task")==true)? cmd.getOptionValue("task"):"blastn";
        boolean fastaonly = cmd.hasOption("fa")==true;

        int partitions = (cmd.hasOption("partitions")==true)? Integer.valueOf(cmd.getOptionValue("partitions")):100;

        sc.hadoopConfiguration().set("textinputformat.record.delimiter", ">");
        JavaRDD<String> rdd;
        if(fastaonly)
            rdd = sc.textFile(input+"/*.fa*");
        else
            rdd = sc.textFile(input); //take whole directory as input

        JavaRDD<String> fastaRDD;

        if(partitions!=0)
            fastaRDD = rdd.map(v->">"+v.trim()).repartition(Integer.valueOf(partitions));
        else fastaRDD = rdd.map(v->">"+v.trim());

        JavaRDD<String> outRDD = fastaRDD.mapPartitions(f -> {
            Process process;
            String query = "";

            while(f.hasNext()){
                query += f.next();
            }
            String blastn_cmd;
            if(task.equalsIgnoreCase("megablast"))
                blastn_cmd = "/usr/bin/blastn -query <(echo -e \"" + query + "\") -db "+db+" -task megablast -word_size "+word_size+" -max_target_seqs "+max_target_seqs+" -evalue "+evalue+" " + ((show_gis == true) ? "-show_gis " : "") + " -outfmt "+outfmt;
            else
                blastn_cmd = "/usr/bin/blastn -query <(echo -e \"" + query + "\") -db "+db+" -word_size "+word_size+" -gapopen "+gapopen+" -gapextend "+gapextend+" -penalty "+penalty+" -reward "+reward+" -max_target_seqs "+max_target_seqs+" -evalue "+evalue+" " + ((show_gis == true) ? "-show_gis " : "") + " -outfmt "+outfmt;
            System.out.println(blastn_cmd);

            //ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", blastn_cmd);
            ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", blastn_cmd);
            process = pb.start();

            BufferedReader inbuff = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            ArrayList<String> out = new ArrayList<String>();
            while ((line = inbuff.readLine()) != null) {
                //System.out.println(line);
                out.add(line);
            }

            BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String e;
            while ((e = err.readLine()) != null) {
                out.add(e);
            }

            //out.add(blastn_cmd);
            process.waitFor();
            inbuff.close();
            return out.iterator();
        });
        outRDD.saveAsTextFile(output);
        sc.stop();
    }
}
