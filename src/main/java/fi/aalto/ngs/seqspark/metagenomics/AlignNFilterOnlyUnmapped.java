package fi.aalto.ngs.seqspark.metagenomics;


import com.github.lindenb.jbwa.jni.BwaIndex;
import com.github.lindenb.jbwa.jni.BwaMem;
import com.github.lindenb.jbwa.jni.ShortRead;
import htsjdk.samtools.*;
import org.apache.commons.cli.*;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.seqdoop.hadoop_bam.FastqInputFormat;
import org.seqdoop.hadoop_bam.FastqOutputFormat;
import org.seqdoop.hadoop_bam.SequencedFragment;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**Usage
 *  spark-submit --master local[4] --class fi.aalto.ngs.seqspark.metagenomics.AlignNFilterOnlyUnmapped target/metagenomics-0.9-jar-with-dependencies.jar -fastq /user/root/fqsplits -ref /index/adhoc.fa -out /user/root/testb

 **/


public class AlignNFilterOnlyUnmapped {

  //unset SPARK_JAVA_OPTS

  private static String alignmentTable = "alignedreads";
  private static String rawreadsTable = "rawreads";

  public static void main(String[] args) throws IOException {
    SparkConf conf = new SparkConf().setAppName("AlignNFilterOnlyUnmapped");
    //conf.set("spark.scheduler.mode", "FAIR");
    //conf.set("spark.scheduler.allocation.file", "/opt/cloudera/parcels/CDH-5.10.0-1.cdh5.10.0.p0.41/etc/hadoop/conf.dist/pools.xml");
      JavaSparkContext sc = new JavaSparkContext(conf);

      //String query = args[2];

      Options options = new Options();
      Option pathOpt = new Option( "in", true, "Path to fastq file in hdfs." );    //gmOpt.setRequired(true);
      Option refOpt = new Option( "ref", true, "Path to fasta reference file in local FS." );
      Option fqoutOpt = new Option( "out", true, "" );
      //Option formatOpt = new Option( "format", true, "bam or sam, fastq is default" );

      options.addOption( pathOpt );
      options.addOption( refOpt );
      options.addOption( fqoutOpt );
      options.addOption(new Option( "partitions", true, "number of file partitions to save, defaults to same as number of input partitions"));
      options.addOption(new Option( "minsize", true, "minsize for partition (in bytes), defaults to 0"));

      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp( "spark-submit <spark specific args>", options, true );

      CommandLineParser parser = new BasicParser();
      CommandLine cmd = null;
      try {
          // parse the command line arguments
          cmd = parser.parse( options, args );
      }
      catch( ParseException exp ) {
          // oops, something went wrong
          System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
      }
      String input = cmd.getOptionValue("in");
      String ref = (cmd.hasOption("ref")==true)? cmd.getOptionValue("ref"):null;
      String outDir = (cmd.hasOption("out")==true)? cmd.getOptionValue("out"):null;
      int numpartitions = (cmd.hasOption("partitions")==true)? Integer.valueOf(cmd.getOptionValue("partitions")):0;
      int minsize = (cmd.hasOption("minsize")==true)? Integer.valueOf(cmd.getOptionValue("minsize")):0;


    JavaPairRDD<Text, SequencedFragment> fastqRDD = sc.newAPIHadoopFile(input, FastqInputFormat.class, Text.class, SequencedFragment.class, sc.hadoopConfiguration());
    //fastqRDD.cache();

    //sc.broadcast(index);
      //This version return ONLY mapped read names so we can filter them later on
      JavaRDD<String> alignmentRDD = fastqRDD.mapPartitions(split -> {
        //THIS MUST BE LOADED HERE IN YARN
        System.loadLibrary("bwajni");
        BwaIndex index = new BwaIndex(new File(ref));
        BwaMem mem = new BwaMem(index);
     
        List<ShortRead> L1 = new ArrayList<ShortRead>();
        List<ShortRead> L2 = new ArrayList<ShortRead>();

          while (split.hasNext()) {
              Tuple2<Text, SequencedFragment> next = split.next();
              String key = next._1.toString();
              //FIXME: With BWA 0.7.12 new Illumina reads fail: [mem_sam_pe] paired reads have different names: "M02086:39:AA7PR:1:1101:6095:20076 1:N:0:1", "M02086:39:AA7PR:1:1101:6095:20076 2:N:0:1"
              String[] keysplit = key.split(" ");
              key = keysplit[0];

              SequencedFragment sf = new SequencedFragment();
              sf.setQuality(new Text(next._2.getQuality().toString()));
              sf.setSequence(new Text(next._2.getSequence().toString()));

              if (split.hasNext()) {

                  Tuple2<Text, SequencedFragment> next2 = split.next();
                  String key2 = next2._1.toString();
                  String[] keysplit2 = key2.split(" ");
                  key2 = keysplit2[0];
                  //key2 = key2.replace(" 2:N:0:1","/2");

                  SequencedFragment sf2 = new SequencedFragment();
                  sf2.setQuality(new Text(next2._2.getQuality().toString()));
                  sf2.setSequence(new Text(next2._2.getSequence().toString()));

                  if(key.equalsIgnoreCase(key2)){
                      L1.add(new ShortRead(key, sf.getSequence().toString().getBytes(), sf.getQuality().toString().getBytes()));
                      L2.add(new ShortRead(key2, sf2.getSequence().toString().getBytes(), sf2.getQuality().toString().getBytes()));
                  }else
                      split.next();
              }
          }

        String[] aligns = mem.align(L1, L2);
        if (aligns != null) {

          return  Arrays.asList(aligns).iterator();
        } else
          return new ArrayList<String>().iterator();
      });

    //List<String> mappedreads = alignmentRDD.collect();
    alignmentRDD.filter(read ->{
      final SAMLineParser samLP = new SAMLineParser(new DefaultSAMRecordFactory(), ValidationStringency.SILENT,  new SAMFileHeader(), null, null);

      SAMRecord record = samLP.parseLine(read);
      if(record.getReadUnmappedFlag())
         return true;
        return false;
    });

    /*if(format!=null)
      new HDFSWriter(alignmentRDD, fastqOutDir, format, sc);
    else*/
      //alignmentRDD.saveAsNewAPIHadoopFile(fastqOutDir, Text.class, SequencedFragment.class, FastqOutputFormat.class, sc.hadoopConfiguration());
      alignmentRDD.saveAsTextFile(outDir);
    sc.stop();

  }


  private static SequencedFragment copySequencedFragment(SequencedFragment sf, String sequence, String quality) {
    SequencedFragment copy = new SequencedFragment();

    copy.setControlNumber(sf.getControlNumber());
    copy.setFilterPassed(sf.getFilterPassed());
    copy.setFlowcellId(sf.getFlowcellId());
    copy.setIndexSequence(sf.getIndexSequence());
    copy.setInstrument(sf.getInstrument());
    copy.setLane(sf.getLane());
    copy.setQuality(new Text(quality));
    copy.setRead(sf.getRead());
    copy.setRunNumber(sf.getRunNumber());
    copy.setSequence(new Text(sequence));
    copy.setTile(sf.getTile());
    copy.setXpos(sf.getXpos());
    copy.setYpos(sf.getYpos());

    return copy;
  }

}