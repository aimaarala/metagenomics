/*

package se.ki.ngs.metagenomics.sparkncbiblast

/**
  * Created by davbzh on 2017-04-18.
  */

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions._
import org.apache.spark.{SparkConf, SparkContext}
import se.ki.ngs.metagenomics.sparkncbiblast.blastStructure._

object sparkBLAST {

  def seqlenth ( sc: SparkContext , input: RDD[String] ) :  DataFrame = {

    val sqlContext = new SQLContext(sc)
    //import sqlContext.implicits._
    import sqlContext.implicits._

    val seqlength = input.map(fseq => fseq.trim().split("\n"))
    .map(fseq => new Seqlen(fseq(0).replace(">",""), fseq.slice(1, fseq.length).mkString("").length,
         fseq.slice(1, fseq.length).mkString(""))
    ).toDF

    return seqlength
  }

  //Function to run nt blast and return dataframe
  def hgmegablast ( sc: SparkContext , seqs: RDD[String], seqlength: DataFrame, Npartitions: Int ) :  RDD[String] = {

    val sqlContext = new SQLContext(sc)
    //import sqlContext.implicits._
    import sqlContext.implicits._

    val megablast =   seqs.map(fquery_agg => blast_cmd(fquery_agg,"megablast"))
      .filter(line => line(0) != null)
      .filter(line => !line(0).equals(""))
      .map(blastres => (new Blastresult(
        blastres(0),
        blastres(1),
        blastres(2).toFloat,
        blastres(3).toInt,
        blastres(4).toInt,
        blastres(5).toInt,
        blastres(6).toInt,
        blastres(7).toInt,
        blastres(8).toInt,
        blastres(9).toInt,
        blastres(10).toFloat,
        blastres(11).toFloat
      )
        )
      ).toDF

    //Register both dataframes
    megablast.registerTempTable("rawreadsTable")
    seqlength.registerTempTable("seqlength")

    //Join megablast with seqlen data
    val megablastdf = megablast.join(seqlength, Seq("qseqid"),  "left_outer")

    //calculate % overlap
    val megablastdf_ovelap = megablastdf.withColumn("pioverlap",  (((col("qend") - col("qstart"))+1)/col("seqlen"))*100)

    //Select sequences with more that 70% identity over 70% overlap
    val megablastdf_final = megablastdf_ovelap.filter($"pident" > 70 && $"pioverlap" > 70)

    //Select sequences that were not similar to human genome
    val eseqlengthleft = seqlength.join(megablastdf_final, Seq("qseqid"), "leftanti")

    //and create fasta RDD
    val nonHG = eseqlengthleft.rdd.map(row => Array(">" + row(0), row(2)))
      .map(line => line.mkString("\n")).repartition(Npartitions)
    return nonHG
  }

  //Function to run nt blast and return dataframe
  def nonhgntbalst ( sc: SparkContext , nonHG: RDD[String] ) :  DataFrame = {

    val sqlContext = new SQLContext(sc)
    //import sqlContext.implicits._
    import sqlContext.implicits._

    val ntres = nonHG.map(fquery_agg => blast_cmd(fquery_agg,"blastn"))
      .filter(line => line(0) != null)
      .filter(line => !line(0).equals(""))
      .map(blastres => (new Blastresult(
        blastres(0),
        blastres(1),
        blastres(2).toFloat,
        blastres(3).toInt,
        blastres(4).toInt,
        blastres(5).toInt,
        blastres(6).toInt,
        blastres(7).toInt,
        blastres(8).toInt,
        blastres(9).toInt,
        blastres(10).toFloat,
        blastres(11).toFloat
      )
        )
      ).toDF

    return ntres
  }

  def main(args: Array[String]) {

    val inputFile = args(0)
    val outputFile = args(1)
    val Npartitions = args(2).toInt //Usually 1200 as we have such amount of cores on our cluster

    /*
    //TODO: SparkSession is better in this case but it has issues reading with hadoop configuration
    val sc = SparkSession
      .builder()
      .appName("sparkBLAST")
      //.config("spark.some.config.option", "some-value")
      .getOrCreate()
    */

    val conf = new SparkConf().setAppName("sparkBLAST")
    // Create a Scala Spark Context.
    val sc = new SparkContext(conf)

    // each sequeence in Fasta format starts with >seqid followded by nucleotide seqiences
    // and sequence part might be split with \n chactater: >seqid\nACTG\nACTT\nACTG
    // So we need to set custom spliter ">" to properly RDD
    sc.hadoopConfiguration.set("textinputformat.record.delimiter",">")
    //sc.conf.set("textinputformat.record.delimiter",">")

    // Load  input fasta.
    val input =  sc.textFile(inputFile)
    //val input =  sc.read.textFile(inputFile)

    //estimate the length of each sequence. We will need it to calculate percen overlaps
    //from blast allignment
    val seqlength = seqlenth(sc, input)

    // Add ">" to each RDD back and trim new line "\n" then repartition them for desired number of RDDs
    // repartition methods is more expecnive but it works comapared to coalesce
    val seqs = input.map(fquery => ">" + fquery.trim()).repartition(Npartitions)
    // first run megablast to clean human genome

    //Clean sequences from human genome and return non hg sequences
    val nonHG = hgmegablast(sc, seqs, seqlength, Npartitions)

    //and conduct blatn abainst
    val nonHGnt = nonhgntbalst(sc, nonHG)

    //myDF.as[(String, Int)].rdd
    //val sqlDF = spark.sql("SELECT * FROM megablastdf_final")
    //.sort(desc("pioverlap"),desc("pident"))

    //Save the dataframe
    nonHGnt.write.format("com.databricks.spark.csv").option("header", "false")
    //.option("codec", "org.apache.hadoop.io.compress.GzipCodec")
    .save(outputFile)

    //nonHG.saveAsTextFile(outputFile)
    sc.stop()
  }
}

 */