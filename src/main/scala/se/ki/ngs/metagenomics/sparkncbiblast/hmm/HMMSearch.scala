package se.ki.ngs.metagenomics.sparkncbiblast.hmm

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.fs.FileStatus
//
import org.apache.spark.{SparkConf, SparkContext}
//
import scala.collection.mutable.ArrayBuffer
import org.rogach.scallop._
import scala.sys.process._

/**
  * Created by davbzh on 2017-05-26.
  */

/*
/srv/hops/spark/bin/spark-submit --master yarn --deploy-mode client --conf spark.scheduler.mode=FAIR --conf spark.shuffle.service.enabled=true --executor-memory 20g  --conf spark.dynamicAllocation.enabled=true --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --class se.ki.ngs.metagenomics.sparkncbiblast.hmm.HMMSearch metagenomics-0.9-jar-with-dependencies.jar --inputDir /Projects/NGS_Projects/test_outputs/Caski_single_contigs_min60_Protein --outputDir /Projects/NGS_Projects/test_outputs/Caski_single_contigs_hmm
*/

object HMMSearch {

  class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
    val inputDir = opt[String]("inputDir", required = true)
    val outputDir = opt[String]("outputDir", required = true)
    verify()
  }


  def main(args: Array[String]) {

    val argconf = new Conf(args)
    val inputDir = argconf.inputDir()
    val outputDir = argconf.outputDir()

    //spark configuration
    val conf = new SparkConf().setAppName("HMMSearch")
    // Create a Scala Spark Context.
    val sc = new SparkContext(conf)
    //hdfs configuration
    val fs = FileSystem.get(new Configuration())
    //list files in the hdfs directroty
    val st: Array[FileStatus] = fs.listStatus(new Path( inputDir ))
    //and add to array
    var splitFileList = ArrayBuffer[String]()
    st.map(file_st => {
      if (!file_st.equals("_SUCCESS")){
        splitFileList.append(file_st.getPath.getName)
      }
    })

    //now parallelise as spark context
    val fastaFilesRDD = sc.parallelize(splitFileList, splitFileList.length)

    //Create output directory
    fs.mkdirs(new Path( outputDir ))
    //and perfomrn hmmsearch
    val hmmRDD = fastaFilesRDD.map(fname => {

      //construct command strings
      val rm_tmp_dir = "if [ -d /mnt/hdfs/1/hmm_tmp_dir ]; then rm -r /mnt/hdfs/1/hmm_tmp_dir; fi"
      val mk_tmp_dir = "if [ ! -d /mnt/hdfs/1/hmm_tmp_dir ]; then mkdir /mnt/hdfs/1/hmm_tmp_dir; fi"
      val cp_2_loc = "/srv/hops/hadoop/bin/hdfs dfs -get " + inputDir + "/" + fname +  " /mnt/hdfs/1/hmm_tmp_dir/" + fname
      val hmm = "hmmsearch --noali --cpu 10 -o /mnt/hdfs/1/hmm_tmp_dir/lsu."+fname+".txt --tblout /mnt/hdfs/1/hmm_tmp_dir/lsu."+fname+".table.txt /mnt/hdfs/1/vFam_A_2014/vFam-A_2014.hmm" + " /mnt/hdfs/1/hmm_tmp_dir/" + fname + " 2> /mnt/hdfs/1/hmm_tmp_dir/error.log"
      val cp_2_dfs = "/srv/hops/hadoop/bin/hdfs dfs -put /mnt/hdfs/1/hmm_tmp_dir/lsu."+fname+".table.txt" + " " + outputDir+"/"+fname+".table.txt";
      //run commands
      val rm_status = Seq("/bin/sh", "-c", rm_tmp_dir).!
      val mk_status = Seq("/bin/sh", "-c", mk_tmp_dir).!
      val cp_2_loc_status = Seq("/bin/sh", "-c", cp_2_loc).!
      val hmm_status = Seq("/bin/sh", "-c", hmm).!
      val cp_2_dfs_status = Seq("/bin/sh", "-c", cp_2_dfs).!


    }).collect()

    val clean_hmmRDD = fastaFilesRDD.map(fname => {

      val rm_tmp_dir = "if [ -d /mnt/hdfs/1/hmm_tmp_dir ]; then rm -r /mnt/hdfs/1/hmm_tmp_dir; fi"
      val clean_up_status = Seq("/bin/sh", "-c", rm_tmp_dir).!

    }).collect()
    //fastaFilesRDD.saveAsTextFile(outputDir + "_test")

  }
}
