package se.ki.ngs.metagenomics.sparkncbiblast

import scala.sys.process._

/**
  * Created by davbzh on 2017-04-30.
  */

object blastStructure {

  def blast_cmd ( query: String, cmd: String  ) :  Array[String] = {

    val stdout = new StringBuilder
    val stderr = new StringBuilder

    if (cmd.equals("blastn")){
      val blastn_cmd = "/usr/bin/blastn -query <(echo -e \"" + query + "\") -db /mnt/hdfs/1/Index_blastn/nt " +
        "-word_size 11 -gapopen 0 -gapextend 2 -penalty -1 -reward 1 -max_target_seqs 10 -evalue 0.001 -outfmt 6 "

      //status variable contains the exit status of the command.
      //The stdout variable contains the STDOUT if the command is successful
      //and stderr contains the STDERR from the command if there are problems.
      val status = Seq("/bin/bash", "-c", blastn_cmd) ! ProcessLogger(stdout append _+"\n", stderr append _)
      //val contents = Process(Seq("/bin/bash", "-c", blastn_cmd)).lineStream
    } else if (cmd.equals("megablast")) {
      val mebablastn_cmd = "/usr/bin/blastn -query <(echo -e \"" + query + "\") -db /mnt/hdfs/1/index_hg/hg  " +
        "-word_size 28 -task megablast -max_target_seqs 5 -evalue 0.0001 -outfmt 6 "
      val status = Seq("/bin/bash", "-c", mebablastn_cmd) ! ProcessLogger(stdout append _+"\n", stderr append _)
    }

    // initiate result array that we will be adding results
    // blast outfmt 6 mast be 12 fields
    val result = new Array[String](12)

    //blast outputs partcial results (half of the line) or several results in  ProcessLogger
    //and it must be checked. TODO: check why "!" sign doesnt't wait?
    //https://www.garysieling.com/scaladoc/scala.sys.process/2016/02/15/scala__sys_process.html
    val outlinelengh = stdout.toString().split("\n").length
    if (outlinelengh > 1){
      for( outlines <- 1 to outlinelengh) {
        while (outlines.toString().split("\n")(0).split("\t").length == 12) {
          val bloutpults =  outlines.toString().split("\n")(0).split("\t")
          result(0) = bloutpults(0)
          result(1) = bloutpults(1)
          result(2) = bloutpults(2)
          result(3) = bloutpults(3)
          result(4) = bloutpults(4)
          result(5) = bloutpults(5)
          result(6) = bloutpults(6)
          result(7) = bloutpults(7)
          result(8) = bloutpults(8)
          result(9) = bloutpults(9)
          result(10) = bloutpults(10)
          result(11) = bloutpults(11)
        }
      }
    } else {
      if (outlinelengh <= 1 & stdout.toString().split("\t").length ==12){
        val bloutpults=  stdout.toString().split("\t")
        result(0) = bloutpults(0)
        result(1) = bloutpults(1)
        result(2) = bloutpults(2)
        result(3) = bloutpults(3)
        result(4) = bloutpults(4)
        result(5) = bloutpults(5)
        result(6) = bloutpults(6)
        result(7) = bloutpults(7)
        result(8) = bloutpults(8)
        result(9) = bloutpults(9)
        result(10) = bloutpults(10)
        result(11) = bloutpults(11)
      }
    }

    return result //stderr
  }

  //Case classes for data fame inputs
  case class Blastresult(
                          val qseqid: String,
                          val sseqid: String,
                          val pident: Float,
                          val length: Int,
                          val mismatch: Int,
                          val gapopen: Int,
                          val qstart: Int,
                          val qend: Int,
                          val sstart: Int,
                          val send: Int,
                          val evalue: Float,
                          val bitscore: Float
                        )

  case class Seqlen(
                     val qseqid: String, val seqlen: Int, val seq: String
                   )


}
