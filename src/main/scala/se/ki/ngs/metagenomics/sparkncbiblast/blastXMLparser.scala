package se.ki.ngs.metagenomics.sparkncbiblast

import scala.xml.XML

/**
  * Created by davbzh on 2017-04-19.
  */

object blastXMLparser {

  def blastXML ( xmliput: String  ) : String =  { //Array[String]
    //http://alvinalexander.com/scala/how-to-load-xml-file-in-scala-load-open-read
    //http://alvinalexander.com/scala/xml-parsing-xpath-extract-xml-tag-attributes

    val xml = XML.loadFile(xmliput)
    //val xml = XML.loadString(xmliput)

    //TODO: I should probably use map type structure here
    val result = new Array[String](6)
    //Check if is better to use mutable ArrayBuffer[String]
    //var fruits = ArrayBuffer[String]()
    //result += "Some strings"

    for( iteritmem <- xml \\ "Iteration" ){

      val queryid  = ( iteritmem \\ "Iteration_query-def" ).text
      val querylen = ( iteritmem \\ "Iteration_query-len" ).text.toInt

      for( hits <- iteritmem \\ "Iteration_hits" ){

        for( hit <- hits \\ "Hit" ){

          val hitgi   = ( hit \\ "Hit_id"  ).text.split("\\|")(1)
          val hitacc  = ( hit \\ "Hit_accession" ).text
          val hitname = ( hit \\ "Hit_def").text

          for( hsps <- hit \\ "Hit_hsps" ){

            val hsp_positives      = ( hsps \\ "Hsp_positive").text.toFloat
            val e_value            = ( hsps \\ "Hsp_evalue").text.toFloat
            val hsp_score          = ( hsps \\ "Hsp_score").text.toFloat
            val subject_start      = ( hsps \\ "Hsp_hit-from").text.toInt
            val subject_end        = ( hsps \\ "Hsp_hit-to").text.toInt
            val query_start        = ( hsps \\ "Hsp_query-from").text.toInt
            val query_end          = ( hsps \\ "Hsp_query-to").text.toInt
            val align_len          = ( hsps \\ "Hsp_hseq").text.replace("-","").length
            val percent_similarity = (hsp_positives/align_len)*100

            var percent_overlap = None: Option[Float]
            if (align_len > querylen || align_len == querylen){
              percent_overlap = Some((100*(((query_end-query_start)+1))).toFloat/(querylen).toFloat)
            } else if (align_len < querylen) {
              percent_overlap = Some((100*(((subject_end-subject_start)+1))).toFloat/(align_len).toFloat)
            }
            //println(queryid + "\t" + querylen + "\t" + hitgi + "\t" + hitacc + "\t"  +  hitname + "\t" +
            //  percent_similarity + "\t" + percent_overlap.getOrElse(None))

            result(0) = queryid
            result(1) = hitgi
            result(2) = hitacc
            result(3) = hitname
            result(4) = percent_similarity.toString
            result(5) = percent_overlap.getOrElse(None).toString

          }
        }
      }
    }
    return result.mkString("\t")
  }
}
