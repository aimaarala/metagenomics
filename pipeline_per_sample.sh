#!/usr/bin/env bash

#See below to set yarn and executor memory options right
#http://stackoverflow.com/questions/38331502/spark-on-yarn-resource-manager-relation-between-yarn-containers-and-spark-execu
#example parameters
#./pipeline_per_sample.sh /Projects/NGS_Projects/2015_F5_AccesHPVNegative12Rubicon /Projects/NGS_Projects/test_outputs 2015_F5_AccesHPVNegative12Rubicon

DEPLOY_MODE=client
SCHEDULER_MODE=FAIR
CLASSPATH=metagenomics-0.9-jar-with-dependencies.jar
INPUT_PATH=${1} #path to HDFS input directory
OUTPUT_PATH=${2} #path to HDFS output directory
PROJECT_NAME=${3} #Used for directory name suffix
REF_INDEX=/mnt/hdfs/1/Index_hg19/hg19_unmasked #path to reference index file in local filesystem of every node
ASSEMBLER_THREADS=10
NORMALIZATION_KMER_LEN=19
NORMALIZATION_CUTOFF=15
TEMP_PATH=${OUTPUT_PATH}/temp  #path to temp directory in HDFS
LOCAL_TEMP_PATH=/mnt/hdfs/1/tmp_outputs #path to temp directory in local filesystem of every node
BLAST_TASK=megablast #other option is blastn which is default
BLAST_DATABASE=/mnt/hdfs/1/Index_blastn/nt #path to local fs
BLAST_HUMAN_DATABASE=/mnt/hdfs/1/index_hg/hg
BLAST_PARTITIONS=100 #repartition contigs to this amount (default is same as number of samples)
BLAST_THREADS=10
#EX_MEM=${4}
#NUM_EX=${7}
#EX_CORES=${8}

#--conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100
#Decompress and interleave all data from HDFS path
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100 --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 10g --conf spark.yarn.executor.memoryOverhead=5000   --class fi.aalto.ngs.seqspark.metagenomics.DecompressInterleave metagenomics-0.9-jar-with-dependencies.jar -in ${INPUT_PATH} -temp ${TEMP_PATH} -out ${OUTPUT_PATH}/${PROJECT_NAME}_interleaved -remtemp

#Align and filter unmapped reads from interleaved reads diretory
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100 --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 30g --conf spark.yarn.executor.memoryOverhead=10000  --class fi.aalto.ngs.seqspark.metagenomics.AlignInterleaved metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_interleaved -out ${OUTPUT_PATH}/${PROJECT_NAME}_aligned -ref ${REF_INDEX}

#Normalize unmapped reads
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100 --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 20g --conf spark.yarn.executor.memoryOverhead=10000  --class fi.aalto.ngs.seqspark.metagenomics.NormalizeRDD metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_aligned -out ${OUTPUT_PATH}/${PROJECT_NAME}_normalized -k ${NORMALIZATION_KMER_LEN} -C ${NORMALIZATION_CUTOFF}

#Group output by samples
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100 --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 10g --conf spark.yarn.executor.memoryOverhead=5000   --class fi.aalto.ngs.seqspark.metagenomics.FastqGroupper metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_normalized -out ${OUTPUT_PATH}/${PROJECT_NAME}_groupped

#Assembly
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100 --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 20g --conf spark.yarn.executor.memoryOverhead=10000  --class fi.aalto.ngs.seqspark.metagenomics.Assemble metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_groupped -out ${OUTPUT_PATH}/${PROJECT_NAME}_assembled -localdir ${LOCAL_TEMP_PATH} -merge -t ${ASSEMBLER_THREADS}

#rename assembled contigs uniquely
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100 --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 10g --conf spark.yarn.executor.memoryOverhead=5000 --class fi.aalto.ngs.seqspark.metagenomics.RenameContigsUniq metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_assembled -out ${OUTPUT_PATH}/${PROJECT_NAME}_contigs -fa fa -partitions ${BLAST_PARTITIONS}

#Blast per assembled sample contig file, megablast used
#spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 20g --conf spark.yarn.executor.memoryOverhead=10000  --class fi.aalto.ngs.seqspark.metagenomics.BlastN metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_assembled/final_contigs -out ${OUTPUT_PATH}/${PROJECT_NAME}_blast -db ${BLAST_DATABASE} -task ${BLAST_TASK} -outfmt 6
#Blast against human db and filter out human matches
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100 --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 10g --conf spark.yarn.executor.memoryOverhead=10000  --class fi.aalto.ngs.seqspark.metagenomics.BlastNFilter metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_contigs -out ${OUTPUT_PATH}/${PROJECT_NAME}_blast_nonhuman -db ${BLAST_HUMAN_DATABASE} -task megablast -outfmt 6 -threshold 70 -num_threads ${BLAST_THREADS}
#Blast non human contigs per file in parallel
spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.cachedExecutorIdleTimeout=100 --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 10g --conf spark.yarn.executor.memoryOverhead=10000  --class fi.aalto.ngs.seqspark.metagenomics.BlastN metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_blast_nonhuman -out ${OUTPUT_PATH}/${PROJECT_NAME}_blast_final -db ${BLAST_DATABASE} -outfmt 6 -num_threads ${BLAST_THREADS}
#Blast non human contigs per RDD partitions in parallel given by BLAST_PARTITIONS param (seems to be slower)
#spark-submit --master yarn --deploy-mode ${DEPLOY_MODE} --conf spark.dynamicAllocation.enabled=true --conf spark.shuffle.service.enabled=true --conf spark.scheduler.mode=${SCHEDULER_MODE} --conf spark.task.maxFailures=100 --conf spark.yarn.max.executor.failures=100 --executor-memory 10g --conf spark.yarn.executor.memoryOverhead=10000  --class fi.aalto.ngs.seqspark.metagenomics.BlastNQuery metagenomics-0.9-jar-with-dependencies.jar -in ${OUTPUT_PATH}/${PROJECT_NAME}_blast_filtered -out ${OUTPUT_PATH}/${PROJECT_NAME}_blast -db ${BLAST_DATABASE} -outfmt 6
